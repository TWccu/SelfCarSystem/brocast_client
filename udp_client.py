import socket
import json

client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
client.bind(("", 23130))

data, addr = client.recvfrom(1024)
payload = json.loads(data.decode('utf-8'))

mqttip = payload['mqttip'] 
mqttport =  payload['mqttport']

print ('MQTT Server ' + mqttip + ':' + str(mqttport))

import socket
import time
import json
import sys, getopt

def get_ip_address():
    ip_address = ''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8",80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address
def main(argv):
    
    easyip = socket.gethostbyname(socket.gethostname())
    mqttip = get_ip_address()
    mqttport = 1883

    if len(argv) > 0:
        opts, args = getopt.getopt(argv, "h:p:", ['mqttip=', 'mqttport='])
        for opt, arg in opts:
            if opt in ('-h', '--mqttip'):
                mqttip = arg

            if opt in ('-p', '--mqttport'):
                mqttport = arg

    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    server.settimeout(0.5)
    server.bind(("", 23131))

    payload = {
        'mqttip': mqttip,
        'mqttport' : mqttport
    }

    message = str(json.dumps(payload))

    try:
        while True:
            server.sendto(message.encode(), ('<broadcast>', 23130))
            print('message payload ' + message)
            time.sleep(1)
    except KeyboardInterrupt:
        print('CTRL + C Interrupted!')

if __name__ == "__main__":
   main(sys.argv[1:])